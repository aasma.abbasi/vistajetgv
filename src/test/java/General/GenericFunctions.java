package General;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * Created by VD-Shireen on 11/5/2018.
 */
public class GenericFunctions {

    public static String generateRandomString() {
        int length = 20;
        String s = RandomStringUtils.randomAlphabetic(length);
        return s;
    }
}
