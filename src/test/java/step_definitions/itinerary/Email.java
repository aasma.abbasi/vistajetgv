package step_definitions.itinerary;

import Core.WebDriverWaits;
import core.WebDriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import pages.Page;

import java.awt.*;

public class Email extends Page {

    public Email() throws AWTException {
    }

    public Itinerary itineraryClass = new Itinerary();

    @Then("^\\[Email] User will be able to see selected itinerary Type on Send Email screen$")
    public void verifyItineraryType() {
        logStep("Verify Itinerary Type on Send Email screen");
        WebDriverWaits.waitUntilElementVisible(By.className("ant-notification-notice-message"));
//        WebDriverWaits.waitUntilElementVisible("ant-notification-notice-message");
        String type = getPageEmail().checkItineraryType().getText();
        Assert.assertEquals(type, itineraryClass.itineraryType);
    }

    @And("^\\[Email] User will be able to Create Itinerary of the selected Type$")
    public void verifyItineraryTypeUpdated() {
        logStep("Verify Itinerary Type Updated on Send Email screen");
        getPageItinerary().getCreateItineraryButton().click();

        WebDriverWaits.waitUntilElementVisible(By.className("ant-notification-notice-message"));
//        WebDriverWaits.waitUntilElementVisible("ant-notification-notice-message");
        String type = getPageEmail().checkItineraryType().getText();
        Assert.assertEquals(type, itineraryClass.itineraryType);
    }

    @Then("^\\[Email] Email will be generated in '([^']*)' form on Send Email screen.$")
    public void verifyEmailForm(String emailForm) {
        logStep("Verify email is generated in " + emailForm + " form");
        if(emailForm == "tabular") {
//            boolean multipleLegCheck = totalOrderCount > 1;
//            Assert.assertTrue(multipleLegCheck);
        }
        else if(emailForm == "descriptive") {
//            boolean multipleLegCheck = totalOrderCount == 1;
//            Assert.assertTrue(multipleLegCheck);
        }
    }

    @Then("^\\[Email] User will be able to see per leg catering message on Email screen$")
    public void verifyCateringMessage() {
        logStep("Verify catering message per leg");
//        boolean multipleLegCheck = totalOrderCount == 1;
//        Assert.assertTrue(multipleLegCheck);
    }
}
