package step_definitions.itinerary;

import core.WebDriverFactory;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import pages.Page;

import java.awt.*;

public class ItineraryDetails extends Page {

    public ItineraryDetails() throws AWTException {
    }

    @Then("^\\[ItineraryDetails] will be able to see applied preference on Order screen$")
    public void selectItineraryWithSentState() throws InterruptedException {
        logStep("User see the Preference");
        Assert.assertTrue(getPageItineraryDetails().getPreferencesAppliedPersonName().isDisplayed());

    }


    @And("^\\[ItineraryDetails] user add adhoc recipient in CC field$")
    public void addUserInCCField() {
        logStep(" Adding adhoc recipient in CC field");
        getPageItineraryDetails().newCCEmailButton().click();
        getPageItineraryDetails().newCCEmailField().sendKeys("raza@vd.com");
        getPageItineraryDetails().newCCEmailField().sendKeys(Keys.ENTER);
    }

    @And("^\\[ItineraryDetails] user add adhoc recipient in BCC field$")
    public void addUserInBCCField()  {
        logStep("Adding adhoc recipient in BCC field");
        getPageItineraryDetails().newBCCEmailButton().click();
        getPageItineraryDetails().newBCCEmailField().sendKeys("Ali@vd.com");
        getPageItineraryDetails().newCCEmailField().sendKeys(Keys.ENTER);
    }

    @Then("^\\[ItineraryDetails] Recipient will be added in CC and BCC$")
    public void verifyUserInBCCField() throws InterruptedException {
       logStep("Verify the recently added recipient in CC field");
       int ccCount = WebDriverFactory.getDriver().findElements(By.className("subscribers-list")).get(1).findElements(By.tagName("span")).size();
       Assert.assertEquals("Verifying Email", "raza@vd.com",WebDriverFactory.getDriver().findElements(By.className("subscribers-list")).get(1).findElements(By.tagName("span")).get(ccCount - 2).getText());
       logStep("Verify the recently added recipient in BCC field");
       int bccCount = WebDriverFactory.getDriver().findElements(By.className("subscribers-list")).get(2).findElements(By.tagName("span")).size();
       Assert.assertEquals("Verifying Email", "Ali@vd.com",WebDriverFactory.getDriver().findElements(By.className("subscribers-list")).get(2).findElements(By.tagName("span")).get(bccCount - 2).getText());
    }

    @And("^\\[ItineraryDetails] user will be able to Email Itinerary$")
    public void clickOnSendEmailItinerary() {
        logStep("Click on Email Itinerary Button");
        getPageItineraryDetails().emailItinerary().click();
    }






}
