package core.dbConnect;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import core.configuration.TestsConfig;
import org.junit.Test;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DbConnect_mySql {

    public static Connection dbConnection() throws SQLException {


        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser(TestsConfig.getConfig().getDbUserNameMySql());
        dataSource.setPassword(TestsConfig.getConfig().getDbPasswordMySql());
        dataSource.setServerName(TestsConfig.getConfig().getDbServerMySql());
        dataSource.setPortNumber(3306);
        Connection conn = dataSource.getConnection();
        return  conn;
    }

    public static String getOrderId() throws SQLException {

        String orderId = "";

        Connection conn = dbConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + TestsConfig.getConfig().getDbConfigMySql() + "_itinerary_itin_ms.itinerary;");
        while (rs.next()){
            orderId = rs.getString("ORDER_ID");
        }
        rs.close();
        stmt.close();
        conn.close();

        return orderId;

    }

    public static String getCustomerName() throws SQLException {

        String custName = "";

        Connection conn = dbConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM " + TestsConfig.getConfig().getDbConfigMySql() + "_itinerary_itin_ms.itinerary;");
        while (rs.next()){
            custName = rs.getString("CUSTOMER_NAME");
        }
        rs.close();
        stmt.close();
        conn.close();

        return custName;

    }

}
