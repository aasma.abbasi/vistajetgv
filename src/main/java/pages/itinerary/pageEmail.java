package pages.itinerary;

import core.WebDriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pages.IPages.IEmail;
import pages.Page;

import java.util.List;

public class pageEmail implements IEmail{

    public pageEmail(Page page){
    }

    int viewsCount = 0;
    private By byItineraryType = By.cssSelector(".ant-col-2:nth-child(2) .col-val");

    public void viewAgainstEachLeg() {
        List <WebElement> views = WebDriverFactory.getDriver().findElement(By.className("flight-summary-card")).findElements(By.linkText("View"));
        viewsCount = views.size();

        for (int i = 0; i < viewsCount; i++) {
            views.get(i).click();

            List <WebElement> additionalItems =  WebDriverFactory.getDriver().findElements(By.className("content-table"));
        }
    }

    public WebElement checkItineraryType() {
        return WebDriverFactory.getDriver().findElement(byItineraryType);
    }




}
