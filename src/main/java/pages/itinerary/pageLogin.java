package pages.itinerary;

import core.WebDriverFactory;
import core.configuration.TestsConfig;
import gherkin.lexer.Th;
import org.openqa.selenium.By;
import pages.IPages.ILogin;
import pages.Page;

public class pageLogin implements ILogin{

    public pageLogin(Page page){

    }
    public static String username = TestsConfig.getConfig().getLoginUserName();
    public static String password = TestsConfig.getConfig().getLoginPassword();

    private By byUserName = By.id("username");
    private By byPassword = By.id("password-input");
    private By byLoginBtn = By.id("login-btn");

    public void login() throws InterruptedException {
        if (WebDriverFactory.getDriver().findElement(byUserName).isDisplayed())
        {
            WebDriverFactory.getDriver().findElement(byUserName).sendKeys(username);
            WebDriverFactory.getDriver().findElement(byPassword).sendKeys(password);
            WebDriverFactory.getDriver().findElement(byLoginBtn).click();
        }
    }
}
