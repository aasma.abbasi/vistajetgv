package pages.IPages;

import org.openqa.selenium.WebElement;

public interface IItinerary {

//    static WebElement getFirstLeg();

    WebElement getCreateItineraryButton();

    WebElement selectItineraryType();

    WebElement clickOnResolveButton();

    WebElement selectAllCheckboxButton();

    WebElement sendEmailItineraryButton();

    WebElement deSelectLegCheckBox();



    //  WebElement preferenceDropDown();
}
