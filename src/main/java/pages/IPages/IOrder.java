package pages.IPages;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface IOrder {
    List<WebElement> getOrderState();

    WebElement getOrderScreen();
}
