package pages.IPages;

import org.openqa.selenium.WebElement;

public interface ISubscriber {

    WebElement getSubscriberButton();

    WebElement getAddSubscriberButton();

    WebElement getAddSubscriberNameTxtbx();

    WebElement getAddSubscriberEmailTxtbx();

    WebElement getAddSubscriberEmailFieldRadio();

    WebElement getAddSubscriberItineraryTypeCheckbx(String type);

}
