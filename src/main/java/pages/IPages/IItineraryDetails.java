package pages.IPages;

import org.openqa.selenium.WebElement;

import java.util.List;

public interface IItineraryDetails {
    WebElement getPreferencesAppliedPersonName();

    WebElement newCCEmailButton();

    WebElement newCCEmailField();

    WebElement newBCCEmailButton();

    WebElement newBCCEmailField();

    WebElement emailItinerary();

    int ccSubscribesList();

    int bccSubscribesList();

    List<WebElement> getCCEmail();

    List<WebElement> getBCCEmail();
}
